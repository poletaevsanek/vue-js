<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::firstOrCreate([
            'name' => 'abissinskaya-koshka',
            'title' => 'Абиссинская кошка',
            'subscription' => 'Порода домашних кошек, выведенная в Великобритании в конце девятнадцатого века на основе аборигенных пород кошек Африки и Юго-Восточной Азии. Абиссинская — одна из самых древних пород кошек и одновременно одна из первых пород, получивших официальный выставочный стандарт.',
            'count' => 10,
            'cost' => 1000,
            'img' => 'img/abissinskaya-koshka.jpg',
            'service_menu_id' => 1,
        ]);
        Post::firstOrCreate([
            'name' => 'avstraliyskaya-dymchataya-koshka',
            'title' => 'Австралийская дымчатая кошка',
            'subscription' => 'Порода домашних кошек, выведенная в Австралии на основе местных дворовых кошек, завезённых сюда, с кошками бирманской и абиссинской породы.',
            'count' => 5,
            'cost' => 1500,
            'img' => 'img/avstraliyskaya-dymchataya-koshka.jpg',
            'service_menu_id' => 1,
        ]);
        Post::firstOrCreate([
            'name' => 'royal-canin-sterilised',
            'title' => 'Royal Canin Sterilised',
            'subscription' => 'Royal Canin Sterilised для стерилизованных кошек и кастрированных котов',
            'count' => 100,
            'cost' => 5164,
            'img' => 'img/royal-canin-sterilised.jpg',
            'service_menu_id' => 2,
        ]);
        Post::firstOrCreate([
            'name' => 'royal-canin-sterilised',
            'title' => 'Royal Canin Sterilised',
            'subscription' => 'Royal Canin Sterilised для стерилизованных кошек и кастрированных котов',
            'count' => 100,
            'cost' => 5164,
            'img' => 'img/royal-canin-sterilised.jpg',
            'service_menu_id' => 2,
        ]);
    }
}
