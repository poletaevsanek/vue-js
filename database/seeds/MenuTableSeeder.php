<?php

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::firstOrCreate([
            'display_name' => 'Кошки',
            'name' => 'koshki',
            'icon' => 'img\koshki.png'
        ]);

        Menu::firstOrCreate([
            'display_name' => 'Собаки',
            'name' => 'sobaki',
            'icon' => 'img\sobaki.png'
        ]);

        Menu::firstOrCreate([
            'display_name' => 'Грызуны',
            'name' => 'gryzuny',
            'icon' => 'img\gryzuny.png'
        ]);

        Menu::firstOrCreate([
            'display_name' => 'Птицы',
            'name' => 'ptitsy',
            'icon' => 'img\ptitsy.png'
        ]);

        Menu::firstOrCreate([
            'display_name' => 'Рыбки',
            'name' => 'rybki',
            'icon' => 'img\rybki.png'
        ]);

        Menu::firstOrCreate([
            'display_name' => 'Ветаптека',
            'name' => 'vetapteka',
            'icon' => 'img\vetapteka.png'
        ]);


    }
}
