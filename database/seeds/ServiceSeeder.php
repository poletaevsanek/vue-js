<?php

use App\Models\ServiceMenu;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceMenu::firstOrCreate([
            'display_name' => 'Кошки',
            'name' => '/koshki',
            'menu_id' => 1
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Корм для кошек',
            'name' => '/korm_dlya_koshki',
            'menu_id' => 1
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Игрушки для кошек',
            'name' => '/igrushki_dlya_koshki',
            'menu_id' => 1
        ]);


        ServiceMenu::firstOrCreate([
            'display_name' => 'Собаки',
            'name' => '/sobaki',
            'menu_id' => 2
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Корм для собаки',
            'name' => '/korm_dlya_sobaki',
            'menu_id' => 2
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Игрушки для собаки',
            'name' => '/igrushki_dlya_sobaki',
            'menu_id' => 2
        ]);


        ServiceMenu::firstOrCreate([
            'display_name' => 'Грызуны',
            'name' => '/gryzuny',
            'menu_id' => 3
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Корм для грызунов',
            'name' => '/korm_dlya_gryzuny',
            'menu_id' => 3
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Игрушки для грызунов',
            'name' => '/igrushki_dlya_gryzuny',
            'menu_id' => 3
        ]);


        ServiceMenu::firstOrCreate([
            'display_name' => 'Птицы',
            'name' => '/ptitsy',
            'menu_id' => 4
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Корм для птиц',
            'name' => '/korm_dlya_ptitsy',
            'menu_id' => 4
        ]);

        ServiceMenu::firstOrCreate([
            'display_name' => 'Рыбки',
            'name' => '/rybki',
            'menu_id' => 5
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Корм для рыбок',
            'name' => '/korm_dlya_rybki',
            'menu_id' => 5
        ]);

        ServiceMenu::firstOrCreate([
            'display_name' => 'Лекарство',
            'name' => '/lecarstvo',
            'menu_id' => 6
        ]);
        ServiceMenu::firstOrCreate([
            'display_name' => 'Витамины',
            'name' => '/vitaminy',
            'menu_id' => 6
        ]);


    }
}
