<?php

namespace App;

use App\Models\Post;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    /**
     * @SWG\Definition(
     *  definition="User",
     *  @SWG\Property(
     *      property="id",
     *      type="integer"
     *  ),
     *  @SWG\Property(
     *      property="name",
     *      type="string"
     *  ),
     *  @SWG\Property(
     *      property="email",
     *      type="string"
     *  ),
     *     @SWG\Property(
     *      property="email_verified_at",
     *      type="timestamp"
     *  ),
     *     @SWG\Property(
     *      property="password",
     *      type="timestamp"
     *  ),
     *    @SWG\Property(
     *      property="remember_token",
     *      type="timestamp"
     *  ),
     *
     *     @SWG\Property(
     *      property="created_at",
     *      type="timestamp"
     *  ),
     *     @SWG\Property(
     *      property="updated_at",
     *      type="timestamp"
     *  )
     * )
     */

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
