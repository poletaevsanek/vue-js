<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Menu",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="display_name",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string"
 *  ),
 *     @SWG\Property(
 *      property="icon",
 *      type="string"
 *  ),
 *     @SWG\Property(
 *      property="created_at",
 *      type="timestamp"
 *  ),
 *     @SWG\Property(
 *      property="updated_at",
 *      type="timestamp"
 *  )
 * )
 */

class Menu extends Model
{
    public function serviceMenus()
    {
        return $this->hasMany(ServiceMenu::class);
    }
}
