<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition(
 *  definition="ServiceMenu",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="display_name",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string"
 *  ),
 *     @SWG\Property(
 *      property="menu_id",
 *      type="integer"
 *  ),
 *     @SWG\Property(
 *      property="created_at",
 *      type="timestamp"
 *  ),
 *     @SWG\Property(
 *      property="updated_at",
 *      type="timestamp"
 *  )
 * )
 */
class ServiceMenu extends Model
{
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
