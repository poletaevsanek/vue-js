<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition(
 *  definition="Post",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="title",
 *      type="string"
 *  ),
 *     @SWG\Property(
 *      property="subscription",
 *      type="longText"
 *  ),
 *     @SWG\Property(
 *      property="count",
 *      type="integer"
 *  ),
 *     @SWG\Property(
 *      property="cost",
 *      type="decimal"
 *  ),
 *   @SWG\Property(
 *      property="img",
 *      type="string"
 *  ),
 *   @SWG\Property(
 *      property="service_menu_id",
 *      type="integer"
 *  ),
 *       @SWG\Property(
 *      property="created_at",
 *      type="timestamp"
 *  ),
 *     @SWG\Property(
 *      property="updated_at",
 *      type="timestamp"
 *  )
 * )
 */
class Post extends Model
{
    //
}
