<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\ServiceMenu;
use App\User;
use Illuminate\Http\Request;

class ServiceMenuController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/catalog/{name}",
     *     summary="Получение товаров категории",
     *     tags={"Get"},
     *     @SWG\Parameter(
     *         name="name",
     *          in="path",
     *         description="Каталог",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Post")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Ошибка",
     *     ),
     * )
     */
    public function index($name)
    {
       $menu = ServiceMenu::where('name', '/'.$name)->first();
       return  $menu->posts;
    }

    /**
     * @SWG\Get(
     *     path="/product/{name}",
     *     summary="Получение конкретного товара",
     *     tags={"Get"},
     *     @SWG\Parameter(
     *         name="name",
     *          in="path",
     *         description="Продукт",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Post")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *        description="Ошибка",
     *     ),
     * )
     */
    public function first($name)
    {
        $post = Post::where('name', $name)->first();
        return  $post;
    }

    /**
     * @SWG\Get(
     *     path="/list/{user}",
     *     summary="История покупок",
     *     tags={"Get"},
     *     @SWG\Parameter(
     *         name="user",
     *          in="path",
     *         description="Пользователь",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Post")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Страница не найдена",
     *     ),
     * )
     */
    public function list($user)
    {
        $user = User::find($user);
        return $user->posts;
    }
}
