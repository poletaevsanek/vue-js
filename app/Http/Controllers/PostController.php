<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/pay",
     *     summary="Оплата товаров, занесение в историю",
     *     tags={"Post"},
     *     @SWG\Parameter(
     *         name="_token",
     *          in="header",
     *         description="Токен",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="user",
     *          in="header",
     *         description="Пользователь",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="post",
     *          in="header",
     *         description="Продукт",
     *         required=true,
     *         type="integer",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Ok",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Post")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Страница не найдена",
     *     ),
     * )
     */
    public function pay(Request $request)
    {
        $posts = explode("|", $request->product);
        $user = User::find($request->user);
        foreach ($posts as $item){
            if(!empty($item)){
                $post = Post::find($item);
                $user->posts()->attach([$post->id]);
            }
        }
        return response()->json(['status'  => 'Ok']);
    }
}
