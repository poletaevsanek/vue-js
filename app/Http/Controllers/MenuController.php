<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/menu",
     *     summary="Получение пунктов меню с категориями",
     *     tags={"Get"},
     *     @SWG\Response(
     *         response=200,
     *         description="success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Menu")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Страница не найдена",
     *     ),
     * )
     */
    public function index()
    {
        return Menu::with(['serviceMenus'])->get();
    }
}
