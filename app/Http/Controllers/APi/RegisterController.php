<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Post(
     *     path="/register",
     *     summary="Регистрация пользователя",
     *     tags={"Post"},
     *     @SWG\Parameter(
     *         name="_token",
     *          in="header",
     *         description="Токен",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *          in="header",
     *          description="Имя пользователя",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *          in="header",
     *          description="Почта пользователя",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *     in="header",
     *          description="Пароль",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="c_password",
     *     in="header",
     *          description="Пароль",
     *         required=true,
     *         type="string",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Пользователь создан",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Ошибка",
     *     ),
     * )
     */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if($validator->fails()){
            return $this->sendError('Ошибка валидации', $validator->errors());
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $success['name'] =  $user->name;
        $success['id'] =  $user->id;
        return $this->sendResponse($success, 'Пользователь создан');
    }

    /**
     * @SWG\Post(
     *     path="/login",
     *     summary="Аунтификация пользователя",
     *     tags={"Post"},
     *     @SWG\Parameter(
     *         name="_token",
     *          in="header",
     *         description="Токен",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *     in="formData",
     *         description="Почта пользователя",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *     in="formData",
     *          description="Пароль",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Страница не найдена",
     *     ),
     * )
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Ошибка валидации', $validator->errors());
        }
        $user = User::where('email', $request->email)->first();
        if(!empty($user) && Hash::check($request->password, $user->password)){
            $success['name'] =  $user->name;
            $success['id'] =  $user->id;
            return $this->sendResponse($success, 'Ok');
        }
        return $this->sendError('Ошибка валидации', ['email' =>'Пользователя не существует']);
    }
}