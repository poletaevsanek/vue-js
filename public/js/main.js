var toastrConfig = {
    warning: function(text, title = ''){
        toastr.warning(text,title, {timeOut: 5000})
    },
    success: function(text, title = ''){
        toastr.success(text,title, {timeOut: 5000})
    },
    error: function(text, title = ''){
        toastr.error(text,title, {timeOut: 5000})
    },
    info: function(text, title = ''){
        toastr.info(text,title, {timeOut: 5000})
    },
};