<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.site.index');
});

Route::post('/register', 'API\RegisterController@register');
Route::post('/login', 'API\RegisterController@login');
Route::post('/pay', 'PostController@pay');

Route::resource('/menu', 'MenuController');

Route::get('/catalog/{name?}', 'ServiceMenuController@index');
Route::get('/product/{name?}', 'ServiceMenuController@first');

Route::get('/list/{user?}', 'ServiceMenuController@list');