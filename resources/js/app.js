require('./bootstrap');


window.Vue = require('vue');


import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import Block from './components/Menu/BlockIndex.vue'
import Menu from './components/Menu/MenuIndex.vue'
import Posts from './components/posts/PostsIndex'
import Cart from './components/posts/Cart';
import Login from './components/Auth/Login';
import Register from './components/Auth/Registr';
import List from './components/posts/List';
import onePost from './components/posts/OneProduct';

const routes = [
    {
        path: '/',
        name: "index",
        components: {
            menuIndex: Menu,
            blockIndex: Block,
        },
    },
    {
        path: '/catalog/:name',
        components: {
            menuIndex: Menu,
            posts: Posts,
        }
    },
    {
        path: '/cart',
        components: {
            menuIndex: Menu,
            cartBlock: Cart,
        }
    },
    {
        path: '/login',
        components: {
            menuIndex: Menu,
            cartBlock: Login,
        }
    },
    {
        path: '/register',
        components: {
            menuIndex: Menu,
            cartBlock: Register,
        }
    },
    {
        path: '/list/',
        components: {
            menuIndex: Menu,
            listBlock: List,
        }
    },
    {
        path: '/product/:name',
        components: {
            menuIndex: Menu,
            posts: onePost,
        }
    },

];

window.store = {
    cart: [],
    cartCount: 0,
    user: null,
};

const router = new VueRouter({routes});

const app = new Vue({
    el: '#app',
    router,
});
