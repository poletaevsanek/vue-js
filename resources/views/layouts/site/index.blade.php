@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <router-view name="blockIndex"></router-view>
            <router-view name="posts"></router-view>
            <router-view name="cartBlock"></router-view>
            <router-view name="listBlock"></router-view>
            <router-view name="oneProduct"></router-view>

        </div>
    </section>
@endsection