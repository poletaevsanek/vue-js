<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{asset('js/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/toaster/toaster.js')}}"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toaster/toaster.css') }}" rel="stylesheet">
    <script src="{{asset('js/main.js')}}"></script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <router-link :to="'/'">  {{ config('app.name', 'Laravel') }}</router-link>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <router-link :to="'/cart'"><i class="fas fa-shopping-cart"></i><span
                                    class="glyphicon glyphicon-shopping-cart" id="countProduct"></span> Корзина
                        </router-link>
                    </li>
                    <!-- Authentication Links -->
                        <li class="nav-item auth">
                            <router-link class="nav-link" :to="'/login'">Вход</router-link>
                        </li>
                        <li class="nav-item auth">
                            <router-link class="nav-link" :to="'/register'">Регистрация</router-link>
                        </li>
                    <li class="nav-item dropdown logout d-none">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><p class="name"></p><span
                                    class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <router-link class="nav-link" :to="'/list'">История покупок</router-link>
                            <a class="dropdown-item" href="javascript:void(0)" onClick="logout()">
                                Выход
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-expand-sm bg-success navbar-dark">
        <router-view name="menuIndex"></router-view>
    </nav>
    <main class="py-4">
        @yield('content')
    </main>
</div>
<script>
   function logout() {
       $('.logout').addClass('d-none');
       $('.auth').removeClass('d-none');
       store.user = null;
   }
</script>
</body>
</html>
